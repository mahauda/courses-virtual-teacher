package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import java.util.Collection;

public interface EntityMapper<T, K> {
	
	/**
	 * Convertir une entité en une DTO
	 * @param entity
	 * @return
	 */
	public K entityToDto(T entity);
	
	/**
	 * Convertir une liste d'entités en une liste de DTOs
	 * @param entities
	 * @return
	 */
	public Collection<K> entitiesToDtos(Collection<T> entities);
	
	/**
	 * Convertir une DTO en une entité
	 * @param dao
	 * @return
	 */
	public T dtoToEntity(K dao);
	
	/**
	 * Convertir une liste de DTOs vers une liste d'entités
	 * @param daos
	 * @return
	 */
	public Collection<T> dtosToEntities(Collection<K> daos);
}
