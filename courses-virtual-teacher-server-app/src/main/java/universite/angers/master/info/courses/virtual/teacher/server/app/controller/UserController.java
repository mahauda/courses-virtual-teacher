package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.email.EmailDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.structure.StructureDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.task.TaskDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.user.UserDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.CourseMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.EmailMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.StructureMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.UserMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.security.SecurityConstants;
import universite.angers.master.info.courses.virtual.teacher.server.app.service.UserService;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.TaskMapper;

@Api("API pour les opérations CRUD sur les utilisateurs")
@RestController
@RequestMapping(value = SecurityConstants.USER_URL)
public class UserController implements EntityController<UserDTO> {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private StructureMapper structureMapper;
	
	@Autowired
	private CourseMapper courseMapper;
	
	@Autowired
	private TaskMapper taskMapper;
	
	@Autowired
	private EmailMapper emailMapper;

	@ApiOperation(value = "Récupèrer la liste complète de tous les utilisateurs", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Collection<UserDTO>> readAll() {
		return ResponseEntity.ok(userMapper.entitiesToDtos(userService.readAll()));
	}

	@ApiOperation(value = "Récupère un utilisateur grâce à son id", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<UserDTO> readById(@PathVariable String id) {
		return ResponseEntity.ok(userMapper.entityToDto(userService.readById(id)));
	}
	
	@ApiOperation(value = "Récupèrer la liste complète de toutes les structures auxquelles il appartient", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "{id}/structures-belong", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<StructureDTO>> readAllStructuresBelong(@PathVariable String id) {
		return ResponseEntity.ok(structureMapper.entitiesToDtos(userService.readAllStructuresBelong(id)));
	}
	
	@ApiOperation(value = "Récupèrer la liste complète de tous les cours qu'il a crée", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "{id}/courses-hold", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<CourseDTO>> readAllCoursesHold(@PathVariable String id) {
		return ResponseEntity.ok(courseMapper.entitiesToDtos(userService.readAllCoursesHold(id)));
	}
	
	@ApiOperation(value = "Récupèrer la liste complète de toutes les tâche qu'il doit réaliser", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "{id}/tasks-perform", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<TaskDTO>> readAllTasksPerform(@PathVariable String id) {
		return ResponseEntity.ok(taskMapper.entitiesToDtos(userService.readAllTasksPerform(id)));
	}
	
	@ApiOperation(value = "Récupèrer la liste complète de toutes les emails qu'il a envoyés", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "{id}/emails-send", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<EmailDTO>> readAllEmailsSend(@PathVariable String id) {
		return ResponseEntity.ok(emailMapper.entitiesToDtos(userService.readAllEmailsSend(id)));
	}
	
	@ApiOperation(value = "Récupèrer la liste complète de toutes les emails qu'il a reçus", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "{id}/emails-receive", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<EmailDTO>> readAllEmailsReceive(@PathVariable String id) {
		return ResponseEntity.ok(emailMapper.entitiesToDtos(userService.readAllEmailsReceive(id)));
	}
	
	@ApiOperation(value = "Ajouter un utilisateur", authorizations = { @Authorization(value="jwtToken") })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO) {
		User user = userService.create(userMapper.dtoToEntity(userDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(userMapper.entityToDto(user));
	}

	@ApiOperation(value = "Mettre à jour un utilisateur en fournissant un id et un utilisateur sous format json", authorizations = { @Authorization(value="jwtToken") })
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<UserDTO> update(@PathVariable String id, @RequestBody UserDTO userDTO) {
		User userOld = userService.readById(id);
		User userNew = userMapper.dtoToEntity(userDTO);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(userMapper.entityToDto(userService.update(userOld)));
	}
	
	@ApiOperation(value = "Supprimer un utilisateurs en fournissant un id", authorizations = { @Authorization(value="jwtToken") })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<?> delete(@PathVariable String id) {
		User user = userService.readById(id);
		userService.delete(user);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}