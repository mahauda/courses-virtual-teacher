package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import org.mapstruct.Mapper;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.structure.StructureDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure.Structure;

@Mapper(componentModel = "spring")
public interface StructureMapper extends EntityMapper<Structure, StructureDTO> {

}
