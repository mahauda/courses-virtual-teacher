package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure.Structure;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.StructureDAO;
import universite.angers.master.info.courses.virtual.teacher.server.app.exceptions.ResourceNotFoundException;
import java.util.Collection;

@Service
public class StructureService implements EntityService<Structure> {

	@Autowired
	private StructureDAO structureDAO;

	@Override
	public Structure create(Structure structure) {
		return structureDAO.save(structure);
	}

	@Override
	public Collection<Structure> readAll() {
		return structureDAO.findAll();
	}

	@Override
	public Structure readById(String id) {
		return structureDAO.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Structure not found for this id : " + id));
	}

	@Override
	public Structure update(Structure structure) {
		return structureDAO.save(structure);
	}

	@Override
	public void delete(Structure structure) {
		structureDAO.delete(structure);
	}
}