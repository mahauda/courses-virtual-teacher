package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.CourseElement;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.CourseElementDAO;
import universite.angers.master.info.courses.virtual.teacher.server.app.exceptions.ResourceNotFoundException;
import java.util.Collection;

@Service
public class CourseElementService implements EntityService<CourseElement> {

	@Autowired
	private CourseElementDAO courseElementDao;

	@Override
	public CourseElement create(CourseElement element) {
		return courseElementDao.save(element);
	}

	@Override
	public Collection<CourseElement> readAll() {
		return courseElementDao.findAll();
	}

	@Override
	public CourseElement readById(String id) {
		return courseElementDao.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Course element not found for this id : " + id));
	}

	@Override
	public CourseElement update(CourseElement element) {
		return courseElementDao.save(element);
	}

	@Override
	public void delete(CourseElement element) {
		courseElementDao.delete(element);
	}
}