package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.Course;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.CourseDAO;
import universite.angers.master.info.courses.virtual.teacher.server.app.exceptions.ResourceNotFoundException;
import java.util.Collection;

@Service
public class CourseService implements EntityService<Course> {

	@Autowired
	private CourseDAO courseDao;

	@Override
	public Course create(Course course) {
		return courseDao.save(course);
	}

	@Override
	public Collection<Course> readAll() {
		return courseDao.findAll();
	}

	@Override
	public Course readById(String id) {
		return courseDao.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Course not found for this id : " + id));
	}

	@Override
	public Course update(Course course) {
		return courseDao.save(course);
	}

	@Override
	public void delete(Course course) {
		courseDao.delete(course);
	}
}