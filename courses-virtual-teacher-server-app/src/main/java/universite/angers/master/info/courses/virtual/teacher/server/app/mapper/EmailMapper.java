package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import org.mapstruct.Mapper;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.email.EmailDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;

@Mapper(componentModel = "spring")
public interface EmailMapper extends EntityMapper<Email, EmailDTO> {

}
