package universite.angers.master.info.courses.virtual.teacher.server.app.security;

public class SecurityConstants {
	
	private SecurityConstants() {
		
	}
	
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String API_URL = "/api";
    public static final String AUTH_URL = "/auth";
    public static final String COURSE_URL = API_URL + "/course";
    public static final String COURSE_ELEMENT_URL = API_URL + "/course-element";
    public static final String EMAIL_URL = API_URL + "/email";
    public static final String STRUCTURE_URL = API_URL + "/structure";
    public static final String TASK_URL = API_URL + "/task";
    public static final String USER_URL = API_URL + "/user";
    public static final String[] SWAGGER_URLS = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };
}