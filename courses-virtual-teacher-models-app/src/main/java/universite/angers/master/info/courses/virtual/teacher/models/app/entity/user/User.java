package universite.angers.master.info.courses.virtual.teacher.models.app.entity.user;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.Course;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure.Structure;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;

@Entity(name = "vtc_user")
@Table(
		name = "vtc_user",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "u_last_name", name="uk_u_last_name"),
				@UniqueConstraint(columnNames = "u_email", name="uk_u_email"),
				@UniqueConstraint(columnNames = "u_phone", name="uk_u_phone"),
				@UniqueConstraint(columnNames = "u_username", name="uk_u_username")
		}
)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class User {

	@Id
	@Column(name = "u_id", nullable = false, insertable = true, updatable = false, 
		columnDefinition = "VARCHAR(36)", length = 36)
	private String id;

	@Enumerated(EnumType.STRING)
	@Column(name = "u_role", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(7)", length = 7)
	private Role role;

	@Column(name = "u_last_name", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String lastname;

	@Column(name = "u_first_name", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String firstname;

	@Temporal(TemporalType.DATE)
	@Column(name = "u_date_birth", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "DATE")
	private Date dateBirth;
	
	@Column(name = "u_email", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String email;
	
	@Column(name = "u_phone", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(10)", length = 10)
	private String phone;
	
	@Column(name = "u_address", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String address;
	
	@Column(name = "u_active", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "BIT", length = 1)
	private boolean active;
	
	@Column(name = "u_username", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String username;

	@Column(name = "u_password", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String password;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(
			name = "vtc_user_belong_structure",
			joinColumns = @JoinColumn(name = "ubs_user_id", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
					foreignKey = @ForeignKey(name = "fk_ubs_user_id")),
			inverseJoinColumns = @JoinColumn(name = "ubs_structure_id", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
					foreignKey = @ForeignKey(name = "fk_ubs_structure_id")))
	private Set<Structure> structuresBelong;

	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, mappedBy = "teacherHold")
	private Set<Course> coursesHold;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "studentPerform")
	private Set<Task> tasksPerform;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "userSend")
	private Set<Email> emailsSend;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(
			name = "vtc_user_receive_email",
			joinColumns = @JoinColumn(name = "ure_user_id", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
					foreignKey = @ForeignKey(name = "fk_ure_user_id")),
			inverseJoinColumns = @JoinColumn(name = "ure_email_id", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
					foreignKey = @ForeignKey(name = "fk_ure_email_id")))
	private Set<Email> emailsReceive;

	public User() {
		this.id = UUID.randomUUID().toString();
		this.dateBirth = new Date();
		this.structuresBelong = new HashSet<>();
		this.coursesHold = new HashSet<>();
		this.tasksPerform = new HashSet<>();
		this.emailsSend = new HashSet<>();
		this.emailsReceive = new HashSet<>();
	}
}
