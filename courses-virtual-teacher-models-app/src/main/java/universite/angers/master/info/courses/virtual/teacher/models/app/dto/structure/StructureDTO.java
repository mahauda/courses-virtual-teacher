package universite.angers.master.info.courses.virtual.teacher.models.app.dto.structure;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class StructureDTO {

	private String id;
	
	private String title;

	private String description;

	private String url;
	
	private int position;

	private boolean expanded;

	private boolean selected;

	private Set<StructureDTO> subStructures;

	public StructureDTO() {
		this.id = UUID.randomUUID().toString();
		this.subStructures = new HashSet<>();
	}
}
