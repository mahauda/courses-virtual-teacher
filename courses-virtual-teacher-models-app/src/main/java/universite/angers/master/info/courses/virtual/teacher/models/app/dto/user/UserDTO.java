package universite.angers.master.info.courses.virtual.teacher.models.app.dto.user;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.Role;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class UserDTO {
	
	private String id;

	private Role role;

	private String lastname;

	private String firstname;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Europe/Paris")
	private Date dateBirth;

	private String email;

	private String phone;
	
	private String address;

	private boolean active;

	private String username;

	private String password;
}
