package universite.angers.master.info.courses.virtual.teacher.models.app.dto.task;

import java.util.Date;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseElementDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.user.UserDTO;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class TaskDTO {

	private String id;
	
	private String title;
	
	private String description;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Europe/Paris")
	private Date dateBegin;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Europe/Paris")
	private Date dateEnd;

	private boolean returned;

	private byte[] archive;
	
	private UserDTO studentPerform;
	
	private CourseElementDTO courseElementDeal;
	
	public TaskDTO() {
		this.id = UUID.randomUUID().toString();
		this.dateBegin = new Date();
		this.dateEnd = new Date();
	}
}
