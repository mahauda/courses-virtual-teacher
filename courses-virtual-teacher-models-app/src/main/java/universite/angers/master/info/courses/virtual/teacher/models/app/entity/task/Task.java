package universite.angers.master.info.courses.virtual.teacher.models.app.entity.task;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.CourseElement;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;

@Entity(name = "vtc_task")
@Table(
		name = "vtc_task",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "t_title", name="uk_t_title"),
		}
)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class Task {

	@Id
	@Column(name = "t_id", nullable = false, insertable = true, updatable = false, 
		columnDefinition = "VARCHAR(36)", length = 36)
	private String id;
	
	@Column(name = "t_title", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(255)", length = 255)
	private String title;

	@Column(name = "t_description", nullable = true, insertable = true, updatable = true, 
		columnDefinition = "TEXT", length = 65535)
	private String description;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "t_date_begin", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "DATETIME")
	private Date dateBegin;

	@Temporal(TemporalType.TIME)
	@Column(name = "t_date_end", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "DATETIME")
	private Date dateEnd;
	
	@Column(name = "t_returned", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "BIT", length = 1)
	private boolean returned;
	
	@Lob @Basic(fetch = FetchType.LAZY)
	@Column(name = "t_archive", nullable = true, insertable = true, updatable = true, 
			columnDefinition = "MEDIUMBLOB", length = 16777215)
	private byte[] archive;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "t_student_perform", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_t_student_perform"))
	private User studentPerform;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "t_course_element_deal", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_t_course_element_deal"))
	private CourseElement courseElementDeal;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "taskConcern")
	private Set<Email> emailsConcern;
	
	public Task() {
		this.id = UUID.randomUUID().toString();
		this.dateBegin = new Date();
		this.dateEnd = new Date();
		this.emailsConcern = new HashSet<>();
	}
}
