import { Injectable } from '@angular/core';
import { Role } from '../models/role.model';
import { ResourceService } from './resource.service';
import { HttpClient } from '@angular/common/http';

/**
 * Service qui permet d'accéder aux opérations CRUD sur les roles utilisateurs
 */
@Injectable({
  providedIn: 'root'
})
export class RoleService extends ResourceService<Role> {

  constructor(httpClient: HttpClient) {
    super(
      httpClient,
      'http://localhost:9220/api',
      'role',
      new Role());
  }
}
