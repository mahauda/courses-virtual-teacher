import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-site-dashboard',
  templateUrl: './site-dashboard.component.html',
  styleUrls: ['./site-dashboard.component.css']
})
export class SiteDashboardComponent implements OnInit {

  constructor(private router: Router) { }

  public ngOnInit(): void {

  }

  RedirectToAccounts(){
    this.router.navigate(['/account']);
  }

  RedirectToCourses(){
    this.router.navigate(['/courses']);
  }

  RedirectToTasks(){
    this.router.navigate(['/tasks']);
  }

  RedirectToStructures(){
    this.router.navigate(['/structures']);
  }

  RedirectToMails(){
    this.router.navigate(['/mails']);
  }


}
