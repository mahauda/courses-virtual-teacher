import { Resource } from './resource.model';
import { Serializable } from './serializable.model';
import { Role } from './role.model';

export class User extends Resource implements Serializable<User> {
    public roles: Role[] = [];
    public firstname: string;
    public lastname: string;
    public username: string;
    public password: string;
    public email: string;
    public phone: string;
    public address: string;
    public birth: Date = new Date();
    public coursesCreated: string[] = [];
    public coursesConsulted: string[] = [];

    /**
     * Convert json to user object
     * @param json 
     */
    public fromJson(json: any): User {
        console.log("json user", json);
         if(json == null) return null;

        var user = new User();
        Object.assign(user, json);
        user.phone = "0602674868";
        user.address = "49000 Angers";
        user.birth = json.birth ? new Date(json.birth as string) : new Date();
        user.roles = [];
        user.coursesCreated = json.coursesCreated ? json.coursesCreated : [];
        user.coursesConsulted = json.coursesConsulted ? json.coursesConsulted : [];

        if(json.roles != null) {
            json.roles.map(r => {
                var role = new Role().fromJson(r);
                user.roles.push(role);
            });
        }

        console.log("object user", user);

        return user;
    }

    /**
     * Convert user object to json
     * @param user 
     */
    public toJson(user: User): any {
        var json = JSON.stringify(user);
        console.log("json user", json);

        return json;
    }
}
